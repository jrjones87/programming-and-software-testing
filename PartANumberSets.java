package MScSoftwareDevelopment;
import java.util.*;

public class Main
{
    private static Scanner stdin = new Scanner(System.in);

    /**
     * Main program that calls the functions and passes parameters.
     */
    public static void main(String[] args)
    {
        int count=0 , countA=0 ,countB = 0;
        int amountA=0, amountB=0;
        int setAAmount=0, setBAmount=0;
        int chosenMenu;
        int[] setA = new int[0];
        int[] setB = new int[0];
        boolean check;


        setAAmount = EnterAAmount(amountA);
        setBAmount = EnterBAmount(amountB);
        setA = SetACount(count,setAAmount,setA);
        setB = SetBCount(count,setBAmount,setB);
        PerformFuntion(chosenMenu = 0,count,countA, countB ,setAAmount,setBAmount,setA,setB, check = false);
    }

    /**
     * User chooses which option to use from list
     * returns integer chosen menu
     */
    static int ChooseFunction()
    {
        int chosenMenu = 0;

        System.out.println();
        System.out.println("*******MENU******");
        System.out.println("1. Display the numbers in both sets");
        System.out.println("2. Display maximum and minimum values");
        System.out.println("3. Display Average of the numbers in each set");
        System.out.println("4. Display if Set A and Set B overlap");
        System.out.println("5. Display intersection of A and B if possible");
        System.out.println("6. Display the set difference of A and B if possible");
        System.out.println("7. Quit");
        System.out.println("Please Select option");

        try
        {

            chosenMenu = stdin.nextInt();
        }
        catch (InputMismatchException e)
        {
            stdin.next();
            System.out.println("only use integers, please select again");
        }
    return chosenMenu;
    }

    /**
     * User enters amount of numbers for set A
     * Amount A the quantity of amount A for set
     * returns the number entered by user for set A
     */
    static int EnterAAmount(int amountA)
    {
        while(true)
        {
            try
            {
            System.out.printf("Please enter amount of numbers for set A: ");
            amountA = stdin.nextInt();
            return amountA;

            }
            catch (InputMismatchException e)
            {
                stdin.next();
                System.out.println();
                System.out.println("Invalid character, please use numbers");
            }
        }
    }

    /**
     * User enters amount of numbers for set B
     * @param amountB the qauntity of amount B for set
     * @return returns the number entered by user for set B
     */
    static int EnterBAmount(int amountB)
    {
        while(true)
        {
            try
            {
                System.out.printf("Please enter amount of numbers for set B: ");
                amountB = stdin.nextInt();
                return amountB;
            }
            catch (InputMismatchException e)
            {
                stdin.next();
                System.out.println();
                System.out.println("Invalid Character, please use integer");

            }
        }
    }

    /**
     * Funtion to Enter set A values
     * Amount and chosen numbers
     * Returns SetA Values
     */
    static int[] SetACount(int count, int setAAmount, int[] setA)
    {
        System.out.println();
        System.out.println("Set A: ");
        setA = new int [setAAmount];
        for (count=0; count < setAAmount; count++)
        {
            {
                try
                {
                    System.out.printf("Select Numbers: ");
                    setA[count] = stdin.nextInt();
                }
                catch (InputMismatchException e)
                {
                    stdin.next();
                    count--;
                    System.out.println("Invalid character please use numbers");
                }
            }
        }
        return setA;
    }

    /**
     * Funtion to Enter set B values
     * Amount and chosen numbers
     * Returns Set B Values
     */
    static int[] SetBCount(int count, int setBAmount, int[] setB)
    {
        System.out.println();
        System.out.println("Set B");
        setB = new int [setBAmount];
        for (count=0; count < setBAmount; count++)
        {
            {
                try
                {
                    System.out.printf("Select Numbers: ");
                    setB[count] = stdin.nextInt();
                }
                catch (InputMismatchException e)
                {
                    stdin.next();
                    count--;
                    System.out.println("Invalid character please use numbers");
                }
            }
        }
        return setB;
    }

    /**
     * Function to perform chosen menu option
     */
    static void PerformFuntion(int chosenMenu,int count,int countA, int countB, int setAAmount,int setBAmount, int[] setA, int[] setB,boolean check)
    {
        int choice;
        boolean end = false;

        while(end == false)
        {
            chosenMenu = ChooseFunction();

            switch (chosenMenu)
            {
                case 1:
                    //System.out.printf("1");
                    DisplaySets(count, setAAmount, setBAmount, setA, setB);
                    break;

                case 2:
                    //System.out.printf("2");
                    MaxNumberA(count,setAAmount,setBAmount,setA);
                    MaxNumberB(count,setAAmount,setBAmount,setB);
                    MinNumberA(count,setAAmount,setBAmount,setA);
                    MinNumberA(count,setAAmount,setBAmount,setB);
                    break;

                case 3:

                    System.out.printf("Calculated the averages\n\r");
                    Averages(count, setAAmount, setBAmount, setA, setB);
                    break;

                case 4:
                    //System.out.printf("4");
                    Overlap(count, setAAmount, setBAmount, setA, setB);
                    break;

                case 5:
                    //System.out.printf("5");

                    Intersection(countA,countB,setAAmount,setBAmount,setA,setB);
                    System.out.println();
                    break;

                case 6:
                    //System.out.printf("6");
                    SetDifference(countA, countB, setAAmount, setBAmount, setA, setB);
                    break;

                case 7:

                    end = CheckEnd(end, check = false);
                    break;

                default:
                    System.out.printf("No Value");
                    break;
            }
        }
    }

    /**
     * Display Numbers in each Set Function
     */
    static void DisplaySets(int count, int setAAmount,int setBAmount, int[] setA, int[] setB)
    {
        System.out.println("Set A number are: ");
        for (count = 0; count < setAAmount; count++)
        {
            System.out.printf("%d ", setA[count]);
        }
        System.out.println();
        System.out.println("Set B number are: ");
        for (count = 0; count < setBAmount; count++)
        {
            System.out.printf("%d ", setB[count]);
        }
        System.out.println();
    }

    /**
     * Funtion to work out averages
     */
    static void Averages(int count, int setAAmount, int setBAmount, int[] setA, int[] setB)
    {
        int totalA=0, totalB=0;

        System.out.println("Set A Average of numbers is:");
        for( count=0; count < setAAmount; count++)
        {
             totalA = totalA + setA[count];

        }
        System.out.printf("%d ", totalA/setAAmount);
        System.out.println();
        System.out.println("Set B Average of numbers is:");
        for( count=0; count < setBAmount; count++)
        {
             totalB = totalB + setB[count];

        }
        System.out.printf("%d ", totalB/setBAmount);
    }

    /**
     * enters function to quit program and end system
     */
    static boolean CheckEnd(boolean end,boolean check)
    {
        int endAnswer = 0;

        while (check == false)
        {
                try
                {
                    System.out.println("Are you sure you want to End");
                    System.out.println("1 for yes");
                    System.out.println("2 for No");
                    endAnswer = stdin.nextInt();

                    if (endAnswer == 1) {
                        check = true;
                        end = true;
                    } else if (endAnswer == 2) {
                        check = true;
                        end = false;
                    } else {
                        System.out.println("Please try again");
                        check = false;
                    }
                } catch (InputMismatchException e)
                {
                    stdin.next();
                    System.out.println("Invalid Choice please use numbers");
                }

        }
        return end;
    }

    /**
     * Function to find Maximum number with Set A numbers
     */
    static int MaxNumberA(int count, int setAAmount, int setBAmount, int[] setA)
    {
        int maxA = setA[0];

        for (count = 0; count < setAAmount; count++) {
            if (setA[count] > maxA) {
                maxA = setA[count];
            }
        }
        System.out.println("The maximum number of set A is: " + maxA);

        return maxA;
    }

    /**
     * Function to find Maximum number with Set B numbers
     */
    static int MaxNumberB(int count, int setAmount, int setBAmount, int[] setB)
    {
        int maxB = setB[0];

        for (count=0; count < setBAmount; count++)
        {
            if (setB[count] > maxB)
            {
                maxB = setB[count];
            }
        }
        System.out.println("The maximum number of set B is: " + maxB);

        return maxB;
    }

    /**
     * Funtion to find Minimum number with Set A numbers
     */
    static int MinNumberA(int count, int setAAmount, int setBAmount, int[] setA)
    {
        int minA = setA[0];

        for (count = 0; count < setAAmount; count++) {
            if (setA[count] < minA) {
                minA = setA[count];
            }
        }
        System.out.println("The minimum number of set A is: " + minA);

        return minA;
    }

    /**
     * Function to find Minimum number with Set B numbers
     */
    static int MinNumberB(int count, int setAAmount, int setBAmount, int[] setB)
    {
        int minB = setB[0];

        for (count=0; count < setBAmount; count++)
        {
            if (setB[count] < minB)
            {
                minB = setB[count];
            }
        }
        System.out.println("The minimum number of set B is: " + minB);

        return minB;
    }

    /**
     * Function to find if there is an overlap
     */
    static void Overlap(int count, int setAAmount, int setBAmount, int[] setA, int[] setB)
    {
        int minA = setA[count];
        int maxA = setA[count];
        int minB = setB[count];
        int maxB = setB[count];

        minA = MinNumberA(count,setAAmount,setBAmount,setA);
        maxA = MaxNumberA(count,setAAmount,setBAmount,setA);
        minB = MinNumberB(count,setAAmount,setBAmount,setB);
        maxB = MaxNumberB(count,setAAmount,setBAmount,setB);

        if((minA < maxB) && (minB < maxA))
        {
            System.out.println("Overlap Found!");
        }
        else
        {
            System.out.println("No Overlap Found!");
        }
    }

    /**
     * Function to find if there is an intersection numbers
     */
    static void Intersection(int countA, int countB, int setAAmount, int setBAmount, int[] setA, int[] setB)
    {
        System.out.println();
        System.out.printf("The intersection is: ");
        for (countA = 0; countA < setAAmount; countA++)
        {
            for (countB =0; countB < setBAmount; countB++)
            {
                if(setA[countA] == setB[countB])
                {
                    System.out.printf(" %d ",setA[countA]);
                }

            }
        }
    }

    /**
     * Function to find if there is a set difference numbers
     */
    static void SetDifference(int countA, int countB, int setAAmount, int setBAmount, int[] setA, int[] setB)
    {
        for (countA = 0; countA < setAAmount; countA++)
        {

            for (countB = 0; countB < setBAmount; countB++)
            {
                {
                    if (setA[countA] != setB[countB])
                    {
                        System.out.printf(" %d ", setA[countA]);
                    }
                }
            }
        }
    }
}
