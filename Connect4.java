package MScSoftwareDevelopment;
import java.util.*;

public class Connect4
{
    public static String playerOneName;
    public static String playerTwoName;

    private static Scanner stdin = new Scanner(System.in);

    public static void main(String[] args)
    {
        PlayerNames();
        String[][] count = CreateDisplay();
        PrintDisplay(count);
        GameInit(count);
    }

    /**
     * Function to initialise connect 4 game
     * Checks which players turn to go.
     * prints Display
     * and enters win checker function
     */
    static void GameInit(String[][] count)
    {
        boolean loop = true;
        int total = 0;

        while(loop)
        {
            if (total % 2 == 0)
            {
                DropRed(count, playerOneName);
            }
            else
            {
                DropYellow(count, playerTwoName);
            }
            total++;

            PrintDisplay(count);

            if (CheckWin(count) != null)
            {
                if (CheckWin(count) == "R")
                {
                    System.out.println(playerOneName + " Wins");
                    loop = false;
                }
                else if (CheckWin(count) == "Y")
                {
                    System.out.println(playerTwoName + " Wins");
                    loop = false;
                }
            }
        }
    }

    /**
     * Function to select players Names
     */
    public static void PlayerNames()
    {
        System.out.println("Player 1: Please enter Name: ");
        playerOneName = stdin.next();
        //System.out.println(PlayerOneName);     //Test

        System.out.println("Player 2: Please enter name: ");
        playerTwoName = stdin.next();
        //System.out.println(PlayerTwoName);    //Test
    }

    /**
     * Function to Create display
     * 2D Array creation.
     */
    private static String[][] CreateDisplay()
    {
        int i,j;
        String[][] count = new String[7][15];

        for (i=0 ; i < count.length; i++)
        {
            for (j=0; j < count[i].length; j++)
            {
                if (j% 2 == 0)
                {
                    count[i][j]= "|";
                }
                else
                {
                    count[i][j]= " ";
                }
                if (i == 6)
                {
                    count[i][1]= "0";
                    count[i][3]= "1";
                    count[i][5]= "2";
                    count[i][7]= "3";
                    count[i][9]= "4";
                    count[i][11]= "5";
                    count[i][13]= "6";
                }
            }
        }
        return count;
    }

    /**
     * Function to Print and display the game grid
     */
    static void PrintDisplay(String[][] count)
    {
        int i,j;

        for (i =0; i<count.length; i++)
        {
            for (j=0; j<count[i].length; j++)
            {
                System.out.print(count[i][j]);
            }
            System.out.println();
        }
    }

    /**
     * Function for Player one to drop Red in column between 0-6
     * Player name string passed through
     */
    static void DropRed(String[][] count, String PlayerOneName)
    {
        int convert = 0;
        int i;
        boolean correctInput = false;

        while (!correctInput)
        {
            try
            {
                System.out.println(PlayerOneName + " Please Drop Red in column (0-6)");
                convert = 2 * stdin.nextInt() + 1;

                if (convert < 14)
                {
                    correctInput = true;
                }
                else
                {
                    System.out.println("Invalid column");
                }
            }
            catch (InputMismatchException e)
            {
                stdin.next();
                System.out.println("Invalid column");
            }
        }

        for (i=5; i>=0; i--)
        {
            if (count[i][convert] == " ")
            {
                count[i][convert] = "R";
                break;
            }
        }
    }

    /**
     * Function for Player one to drop Yellow in column between 0-6
     * Player name string passed through
     */
    static void DropYellow (String[][] count, String PlayerTwoName)
    {
        int convert = 0;
        int i;
        boolean correctInput = false;

        while (!correctInput)
        {
            try
            {
                System.out.println(PlayerTwoName + " Please Drop Yellow in column (0-6)");
                convert = 2 * stdin.nextInt() + 1;

                if (convert < 14)
                {
                    correctInput = true;
                }
                else
                {
                    System.out.println("Invalid column");
                }
            }
            catch (InputMismatchException e)
            {
                stdin.next();
                System.out.println("Invalid column");
            }
        }

        for (i=5; i>=0; i--)
        {
            if (count[i][convert] == " ")
            {
                count[i][convert] = "Y";
                break;
            }
        }
    }

    /**
     * Function to check win in all directions
     */
    static String CheckWin(String[][] count)
    {
        int i;
        int j;

        for (i = 0; i < 6; i++)
        {
            for (j = 0; j < 7; j += 2)
            {
                if ((count[i][j + 1] != " ") && (count[i][j + 3] != " ") && (count[i][j + 5] != " ")
                        && (count[i][j + 7] != " ") && ((count[i][j + 1] == count[i][j + 3])
                        && (count[i][j + 3] == count[i][j + 5]) && (count[i][j + 5] == count[i][j + 7])))
                {
                    return count[i][j + 1];
                }
            }
        }

        for (i = 1; i < 15; i += 2)
        {

            for (j = 0; j < 3; j++)
            {
                if ((count[j][i] != " ") && (count[j + 1][i] != " ") && (count[j + 2][i] != " ")
                        && (count[j + 3][i] != " ") && ((count[j][i] == count[j + 1][i])
                        && (count[j + 1][i] == count[j + 2][i]) && (count[j + 2][i] == count[j + 3][i])))
                {
                    return count[j][i];
                }
            }
        }

        for (i = 0; i < 3; i++)
        {
            for (j = 1; j < 9; j += 2)
            {
                if ((count[i][j] != " ") && (count[i + 1][j + 2] != " ") && (count[i + 2][j + 4] != " ")
                        && (count[i + 3][j + 6] != " ") && ((count[i][j] == count[i + 1][j + 2])
                        && (count[i + 1][j + 2] == count[i + 2][j + 4]) && (count[i + 2][j + 4] == count[i + 3][j + 6])))
                {
                    return count[i][j];
                }
            }
        }

        for (i = 0; i < 3; i++)
        {
            for (j = 7; j < 15; j += 2)
                {
                    if ((count[i][j] != " ") && (count[i + 1][j - 2] != " ") && (count[i + 2][j - 4] != " ")
                            && (count[i + 3][j - 6] != " ") && ((count[i][j] == count[i + 1][j - 2])
                            && (count[i + 1][j - 2] == count[i + 2][j - 4]) && (count[i + 2][j - 4] == count[i + 3][j - 5])))
                    {
                        return count[i][j];
                    }
                }
            }
        return null;
        }
    }

